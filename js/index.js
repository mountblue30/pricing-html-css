  function result() {
    let monthlyPrices = ["19.99", "24.99", "39.99"];
    let yearlyPrices = ["199.99", "249.99", "399.99"];
    let prices = document.getElementsByClassName("price");
    if (prices[0].innerHTML === monthlyPrices[0]) {
      for(index in prices){
        prices[index].innerHTML = yearlyPrices[index];
      }
    } else {
      for (index in prices) {
        prices[index].innerHTML = monthlyPrices[index];
      }
    }
  }